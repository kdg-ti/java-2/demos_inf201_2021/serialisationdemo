package be.kdg.java2;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Logger;

public class Main {
    private static final Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        /*Student student = new Student("Jos", 176);
        try {
            ObjectOutputStream outputStream =
                    new ObjectOutputStream(new FileOutputStream("student.ser"));
            outputStream.writeObject(student);
            outputStream.close();
        } catch (IOException e) {
            logger.severe("file schrijven niet gelukt!");
            e.printStackTrace();
        }*/
        /*try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("student.ser"));
            Student student = (Student) objectInputStream.readObject();
            System.out.println(student);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }*/
        try {
            Connection connection =
                    DriverManager.getConnection("jdbc:hsqldb:file:dbData/demo",
                            "sa","");
            Statement statement = connection.createStatement();
           /* statement.execute("DROP TABLE IF EXISTS STUDENTS");
            statement.execute("CREATE TABLE STUDENTS(ID INTEGER NOT NULL IDENTITY," +
                    "NAME VARCHAR(100) NOT NULL)");
            statement.execute("INSERT INTO STUDENTS VALUES(NULL, 'JOSKE')");
            statement.execute("INSERT INTO STUDENTS VALUES(NULL, 'MARIE')");
            statement.execute("INSERT INTO STUDENTS VALUES(NULL, 'JEF')");*/
            ResultSet resultSet = statement.executeQuery("SELECT * FROM STUDENTS");
            ArrayList<Student> students = new ArrayList<>();
            while (resultSet.next()) {
                int id = resultSet.getInt("ID");
                String name = resultSet.getString("NAME");
                students.add(new Student(id, name, 0, 0));
            }
            statement.close();
            connection.close();
            students.forEach(System.out::println);
        } catch (SQLException e) {
            logger.severe("Can't connect to the database!");
            e.printStackTrace();
        }
    }
}
