package be.kdg.java2;

import java.io.Serializable;
import java.util.logging.Logger;

public class Student implements Serializable {
    private static final Logger logger = Logger.getLogger(Student.class.getName());

    public static final long serialVersionUID = 1L;

    private int id;
    private String firstname;
    private int weight;
    private int length;

    public Student(int id, String firstname, int weight, int length) {
        this.id = id;
        this.firstname = firstname;
        this.weight = weight;
        this.length = length;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id  +
                ", firstname='" + firstname + '\'' +
                ", weight=" + weight +
                ", length=" + length +
                '}';
    }
}
